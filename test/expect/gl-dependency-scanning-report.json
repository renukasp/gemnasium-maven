{
  "version": "2.3",
  "vulnerabilities": [
    {
      "category": "dependency_scanning",
      "name": "Deserialization of Untrusted Data",
      "message": "Deserialization of Untrusted Data in com.fasterxml.jackson.core/jackson-databind",
      "description": "FasterXML jackson-databind allows unauthenticated remote code execution because of an incomplete fix for the CVE-2017-7525 deserialization flaw. This is exploitable by sending maliciously crafted JSON input to the readValue method of the ObjectMapper, bypassing a blacklist that is ineffective if the c3p0 libraries are available in the classpath.",
      "cve": "app/pom.xml:com.fasterxml.jackson.core/jackson-databind:gemnasium:0a647516-66dc-4381-9da7-601193d849e6",
      "severity": "Unknown",
      "solution": "Upgrade to versions 2.8.11.1, 2.9.5 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "app/pom.xml",
        "dependency": {
          "package": {
            "name": "com.fasterxml.jackson.core/jackson-databind"
          },
          "version": "2.9.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-0a647516-66dc-4381-9da7-601193d849e6",
          "value": "0a647516-66dc-4381-9da7-601193d849e6",
          "url": "https://deps.sec.gitlab.com/packages/maven/com.fasterxml.jackson.core/jackson-databind/versions/2.9.2/advisories"
        },
        {
          "type": "cve",
          "name": "CVE-2018-7489",
          "value": "CVE-2018-7489",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-7489"
        }
      ],
      "links": [
        {
          "url": "http://www.oracle.com/technetwork/security-advisory/cpuapr2018-3678067.html"
        },
        {
          "url": "http://www.securityfocus.com/bid/103203"
        },
        {
          "url": "http://www.securitytracker.com/id/1040693"
        },
        {
          "url": "https://github.com/FasterXML/jackson-databind/issues/1931"
        },
        {
          "url": "https://security.netapp.com/advisory/ntap-20180328-0001/"
        },
        {
          "url": "https://www.debian.org/security/2018/dsa-4190"
        }
      ]
    },
    {
      "category": "dependency_scanning",
      "name": "Deserialization of Untrusted Data",
      "message": "Deserialization of Untrusted Data in com.fasterxml.jackson.core/jackson-databind",
      "description": "FasterXML jackson-databind allows unauthenticated remote code execution because of an incomplete fix for the CVE-2017-7525 and CVE-2017-17485 deserialization flaws. This is exploitable via two different gadgets that bypass a blacklist.",
      "cve": "app/pom.xml:com.fasterxml.jackson.core/jackson-databind:gemnasium:0e5fec86-8e66-474c-9e93-b7e519017fe3",
      "severity": "Unknown",
      "solution": "Upgrade to versions 2.8.11.1, 2.9.4 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "app/pom.xml",
        "dependency": {
          "package": {
            "name": "com.fasterxml.jackson.core/jackson-databind"
          },
          "version": "2.9.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-0e5fec86-8e66-474c-9e93-b7e519017fe3",
          "value": "0e5fec86-8e66-474c-9e93-b7e519017fe3",
          "url": "https://deps.sec.gitlab.com/packages/maven/com.fasterxml.jackson.core/jackson-databind/versions/2.9.2/advisories"
        },
        {
          "type": "cve",
          "name": "CVE-2018-5968",
          "value": "CVE-2018-5968",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-5968"
        }
      ],
      "links": [
        {
          "url": "https://github.com/FasterXML/jackson-databind/issues/1899"
        },
        {
          "url": "https://security.netapp.com/advisory/ntap-20180423-0002/"
        },
        {
          "url": "https://www.debian.org/security/2018/dsa-4114"
        }
      ]
    },
    {
      "category": "dependency_scanning",
      "name": "Improper Input Validation",
      "message": "Improper Input Validation in com.fasterxml.jackson.core/jackson-databind",
      "description": "`SubTypeValidator.java` in FasterXML jackson-databind mishandles default typing when ehcache is used (because of `net.sf.ehcache.transaction.manager.DefaultTransactionManagerLookup`), leading to remote code execution.",
      "cve": "app/pom.xml:com.fasterxml.jackson.core/jackson-databind:gemnasium:2e639b4f-f53c-4a3e-a91f-d9731e93c4bc",
      "severity": "Unknown",
      "solution": "Upgrade to versions 2.7.9.6, 2.8.11.4, 2.9.9.2 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "app/pom.xml",
        "dependency": {
          "package": {
            "name": "com.fasterxml.jackson.core/jackson-databind"
          },
          "version": "2.9.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-2e639b4f-f53c-4a3e-a91f-d9731e93c4bc",
          "value": "2e639b4f-f53c-4a3e-a91f-d9731e93c4bc",
          "url": "https://deps.sec.gitlab.com/packages/maven/com.fasterxml.jackson.core/jackson-databind/versions/2.9.2/advisories"
        },
        {
          "type": "cve",
          "name": "CVE-2019-14379",
          "value": "CVE-2019-14379",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14379"
        }
      ],
      "links": [
        {
          "url": "https://github.com/FasterXML/jackson-databind/issues/2387"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-14379"
        }
      ]
    },
    {
      "category": "dependency_scanning",
      "name": "Improper Input Validation",
      "message": "Improper Input Validation in com.fasterxml.jackson.core/jackson-databind",
      "description": "A Polymorphic Typing issue was discovered in FasterXML jackson-databind. When Default Typing is enabled (either globally or for a specific property) for an externally exposed JSON endpoint and the service has the `commons-dbcp` jar in the classpath, and an attacker can find an RMI service endpoint to access, it is possible to make the service execute a malicious payload. This issue exists because of `org.apache.commons.dbcp.datasources.SharedPoolDataSource` and `org.apache.commons.dbcp.datasources.PerUserPoolDataSource` mishandling.",
      "cve": "app/pom.xml:com.fasterxml.jackson.core/jackson-databind:gemnasium:523eaaba-031d-4454-9cc9-d6b0d6753d40",
      "severity": "Unknown",
      "solution": "Upgrade to version 2.9.10.1 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "app/pom.xml",
        "dependency": {
          "package": {
            "name": "com.fasterxml.jackson.core/jackson-databind"
          },
          "version": "2.9.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-523eaaba-031d-4454-9cc9-d6b0d6753d40",
          "value": "523eaaba-031d-4454-9cc9-d6b0d6753d40",
          "url": "https://deps.sec.gitlab.com/packages/maven/com.fasterxml.jackson.core/jackson-databind/versions/2.9.2/advisories"
        },
        {
          "type": "cve",
          "name": "CVE-2019-16942",
          "value": "CVE-2019-16942",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-16942"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-16942"
        }
      ]
    },
    {
      "category": "dependency_scanning",
      "name": "Improper Input Validation",
      "message": "Improper Input Validation in com.fasterxml.jackson.core/jackson-databind",
      "description": "A Polymorphic Typing issue was discovered in FasterXML jackson-databind. When Default Typing is enabled (either globally or for a specific property) for an externally exposed JSON endpoint and the service has the `p6spy` jar in the classpath, and an attacker can find an RMI service endpoint to access, it is possible to make the service execute a malicious payload. This issue exists because of `com.p6spy.engine.spy.P6DataSource` mishandling.",
      "cve": "app/pom.xml:com.fasterxml.jackson.core/jackson-databind:gemnasium:5ee60948-63a0-47a6-8807-378df68649fe",
      "severity": "Unknown",
      "solution": "Upgrade to version 2.9.10.1 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "app/pom.xml",
        "dependency": {
          "package": {
            "name": "com.fasterxml.jackson.core/jackson-databind"
          },
          "version": "2.9.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-5ee60948-63a0-47a6-8807-378df68649fe",
          "value": "5ee60948-63a0-47a6-8807-378df68649fe",
          "url": "https://deps.sec.gitlab.com/packages/maven/com.fasterxml.jackson.core/jackson-databind/versions/2.9.2/advisories"
        },
        {
          "type": "cve",
          "name": "CVE-2019-16943",
          "value": "CVE-2019-16943",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-16943"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-16943"
        }
      ]
    },
    {
      "category": "dependency_scanning",
      "name": "Information Disclosure",
      "message": "Information Disclosure in com.fasterxml.jackson.core/jackson-databind",
      "description": "A Polymorphic Typing issue was discovered in FasterXML jackson-databind. When Default Typing is enabled (either globally or for a specific property) for an externally exposed JSON endpoint and the service has JDOM in the classpath, an attacker can send a specifically crafted JSON message that allows them to read arbitrary local files on the server.",
      "cve": "app/pom.xml:com.fasterxml.jackson.core/jackson-databind:gemnasium:64a8d8c4-3e77-4d6b-a195-f4e2f93d95fe",
      "severity": "Unknown",
      "solution": "Upgrade to versions 2.7.9.6, 2.8.11.4, 2.9.9.1 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "app/pom.xml",
        "dependency": {
          "package": {
            "name": "com.fasterxml.jackson.core/jackson-databind"
          },
          "version": "2.9.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-64a8d8c4-3e77-4d6b-a195-f4e2f93d95fe",
          "value": "64a8d8c4-3e77-4d6b-a195-f4e2f93d95fe",
          "url": "https://deps.sec.gitlab.com/packages/maven/com.fasterxml.jackson.core/jackson-databind/versions/2.9.2/advisories"
        },
        {
          "type": "cve",
          "name": "CVE-2019-12814",
          "value": "CVE-2019-12814",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-12814"
        }
      ],
      "links": [
        {
          "url": "https://github.com/FasterXML/jackson-databind/issues/2341"
        },
        {
          "url": "https://lists.debian.org/debian-lts-announce/2019/06/msg00019.html"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-12814"
        }
      ]
    },
    {
      "category": "dependency_scanning",
      "name": "Improper Input Validation",
      "message": "Improper Input Validation in com.fasterxml.jackson.core/jackson-databind",
      "description": "A Polymorphic Typing issue was discovered in FasterXML jackson-databind. It is related to `com.zaxxer.hikari.HikariDataSource`. This is a different vulnerability than CVE-2019-14540.",
      "cve": "app/pom.xml:com.fasterxml.jackson.core/jackson-databind:gemnasium:97c5173b-cc37-461f-9dd2-399c1f3f474c",
      "severity": "Unknown",
      "solution": "Upgrade to version 2.9.10 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "app/pom.xml",
        "dependency": {
          "package": {
            "name": "com.fasterxml.jackson.core/jackson-databind"
          },
          "version": "2.9.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-97c5173b-cc37-461f-9dd2-399c1f3f474c",
          "value": "97c5173b-cc37-461f-9dd2-399c1f3f474c",
          "url": "https://deps.sec.gitlab.com/packages/maven/com.fasterxml.jackson.core/jackson-databind/versions/2.9.2/advisories"
        },
        {
          "type": "cve",
          "name": "CVE-2019-16335",
          "value": "CVE-2019-16335",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-16335"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-16335"
        }
      ]
    },
    {
      "category": "dependency_scanning",
      "name": "Information Exposure",
      "message": "Information Exposure in com.fasterxml.jackson.core/jackson-databind",
      "description": "A Polymorphic Typing issue was discovered in FasterXML jackson-databind. When Default Typing is enabled (either globally or for a specific property) for an externally exposed JSON endpoint, the service has the `mysql-connector-java` jar in the classpath, and an attacker can host a crafted MySQL server reachable by the victim, an attacker can send a crafted JSON message that allows them to read arbitrary local files on the server. This occurs because of missing `com.mysql.cj.jdbc.admin.MiniAdmin` validation.",
      "cve": "app/pom.xml:com.fasterxml.jackson.core/jackson-databind:gemnasium:9df2bd87-497d-468f-8006-c980375634fa",
      "severity": "Unknown",
      "solution": "Upgrade to versions 2.7.9.6, 2.8.11.4, 2.9.9 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "app/pom.xml",
        "dependency": {
          "package": {
            "name": "com.fasterxml.jackson.core/jackson-databind"
          },
          "version": "2.9.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-9df2bd87-497d-468f-8006-c980375634fa",
          "value": "9df2bd87-497d-468f-8006-c980375634fa",
          "url": "https://deps.sec.gitlab.com/packages/maven/com.fasterxml.jackson.core/jackson-databind/versions/2.9.2/advisories"
        },
        {
          "type": "cve",
          "name": "CVE-2019-12086",
          "value": "CVE-2019-12086",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-12086"
        }
      ],
      "links": [
        {
          "url": "http://www.securityfocus.com/bid/109227"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-12086"
        }
      ]
    },
    {
      "category": "dependency_scanning",
      "name": "Improper Input Validation",
      "message": "Improper Input Validation in com.fasterxml.jackson.core/jackson-databind",
      "description": "A Polymorphic Typing issue was discovered in FasterXML jackson-databind. It is related to `com.zaxxer.hikari.HikariConfig`.",
      "cve": "app/pom.xml:com.fasterxml.jackson.core/jackson-databind:gemnasium:dccd96de-e4ca-4391-bb31-64a1f1c97904",
      "severity": "Unknown",
      "solution": "Upgrade to version 2.9.10 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "app/pom.xml",
        "dependency": {
          "package": {
            "name": "com.fasterxml.jackson.core/jackson-databind"
          },
          "version": "2.9.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-dccd96de-e4ca-4391-bb31-64a1f1c97904",
          "value": "dccd96de-e4ca-4391-bb31-64a1f1c97904",
          "url": "https://deps.sec.gitlab.com/packages/maven/com.fasterxml.jackson.core/jackson-databind/versions/2.9.2/advisories"
        },
        {
          "type": "cve",
          "name": "CVE-2019-14540",
          "value": "CVE-2019-14540",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14540"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-14540"
        }
      ]
    },
    {
      "category": "dependency_scanning",
      "name": "Information Exposure",
      "message": "Information Exposure in com.fasterxml.jackson.core/jackson-databind",
      "description": "A Polymorphic Typing issue was discovered in FasterXML jackson-databind. This occurs when Default Typing is enabled (either globally or for a specific property) for an externally exposed JSON endpoint and the service has the logback jar in the classpath.",
      "cve": "app/pom.xml:com.fasterxml.jackson.core/jackson-databind:gemnasium:ea403343-8d37-430d-9238-e27386f2843b",
      "severity": "Unknown",
      "solution": "Upgrade to versions 2.7.9.6, 2.8.11.4, 2.9.9.2 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "app/pom.xml",
        "dependency": {
          "package": {
            "name": "com.fasterxml.jackson.core/jackson-databind"
          },
          "version": "2.9.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-ea403343-8d37-430d-9238-e27386f2843b",
          "value": "ea403343-8d37-430d-9238-e27386f2843b",
          "url": "https://deps.sec.gitlab.com/packages/maven/com.fasterxml.jackson.core/jackson-databind/versions/2.9.2/advisories"
        },
        {
          "type": "cve",
          "name": "CVE-2019-14439",
          "value": "CVE-2019-14439",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14439"
        }
      ],
      "links": [
        {
          "url": "https://github.com/FasterXML/jackson-databind/issues/2389"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-14439"
        }
      ]
    },
    {
      "category": "dependency_scanning",
      "name": "Deserialization of Untrusted Data",
      "message": "Deserialization of Untrusted Data in com.fasterxml.jackson.core/jackson-databind",
      "description": "FasterXML jackson-databind might allow attackers to have a variety of impacts by leveraging failure to block the `logback-core` class from polymorphic deserialization. Depending on the classpath content, remote code execution may be possible.",
      "cve": "app/pom.xml:com.fasterxml.jackson.core/jackson-databind:gemnasium:ee5e6999-23b2-476b-ab3b-819a4e06724a",
      "severity": "Unknown",
      "solution": "Upgrade to versions 2.7.9.6, 2.8.11.4, 2.9.9.1 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "app/pom.xml",
        "dependency": {
          "package": {
            "name": "com.fasterxml.jackson.core/jackson-databind"
          },
          "version": "2.9.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-ee5e6999-23b2-476b-ab3b-819a4e06724a",
          "value": "ee5e6999-23b2-476b-ab3b-819a4e06724a",
          "url": "https://deps.sec.gitlab.com/packages/maven/com.fasterxml.jackson.core/jackson-databind/versions/2.9.2/advisories"
        },
        {
          "type": "cve",
          "name": "CVE-2019-12384",
          "value": "CVE-2019-12384",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-12384"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-12384"
        }
      ]
    },
    {
      "category": "dependency_scanning",
      "name": "Improper Input Validation",
      "message": "Improper Input Validation in com.fasterxml.jackson.core/jackson-databind",
      "description": "A Polymorphic Typing issue was discovered in FasterXML jackson-databind. It is related to `net.sf.ehcache.hibernate.EhcacheJtaTransactionManagerLookup`.",
      "cve": "app/pom.xml:com.fasterxml.jackson.core/jackson-databind:gemnasium:fb4fd9b5-f692-49b5-9cf4-ca82958f2a53",
      "severity": "Unknown",
      "solution": "Upgrade to version 2.9.10 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "app/pom.xml",
        "dependency": {
          "package": {
            "name": "com.fasterxml.jackson.core/jackson-databind"
          },
          "version": "2.9.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-fb4fd9b5-f692-49b5-9cf4-ca82958f2a53",
          "value": "fb4fd9b5-f692-49b5-9cf4-ca82958f2a53",
          "url": "https://deps.sec.gitlab.com/packages/maven/com.fasterxml.jackson.core/jackson-databind/versions/2.9.2/advisories"
        },
        {
          "type": "cve",
          "name": "CVE-2019-17267",
          "value": "CVE-2019-17267",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-17267"
        }
      ],
      "links": [
        {
          "url": "https://github.com/FasterXML/jackson-databind/issues/2460"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-17267"
        }
      ]
    },
    {
      "category": "dependency_scanning",
      "name": "Improper Input Validation",
      "message": "Improper Input Validation in com.fasterxml.jackson.core/jackson-databind",
      "description": "A Polymorphic Typing issue was discovered in FasterXML jackson-databind. When Default Typing is enabled (either globally or for a specific property) for an externally exposed JSON endpoint and the service has the `apache-log4j-extra` in the classpath, and an attacker can provide a JNDI service to access, it is possible to make the service execute a malicious payload.",
      "cve": "app/pom.xml:com.fasterxml.jackson.core/jackson-databind:gemnasium:fc79306c-cbe4-47bd-80a9-d2610a560930",
      "severity": "Unknown",
      "solution": "Upgrade to version 2.9.10.1 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "app/pom.xml",
        "dependency": {
          "package": {
            "name": "com.fasterxml.jackson.core/jackson-databind"
          },
          "version": "2.9.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-fc79306c-cbe4-47bd-80a9-d2610a560930",
          "value": "fc79306c-cbe4-47bd-80a9-d2610a560930",
          "url": "https://deps.sec.gitlab.com/packages/maven/com.fasterxml.jackson.core/jackson-databind/versions/2.9.2/advisories"
        },
        {
          "type": "cve",
          "name": "CVE-2019-17531",
          "value": "CVE-2019-17531",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-17531"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-17531"
        }
      ]
    },
    {
      "category": "dependency_scanning",
      "name": "DoS by CPU exhaustion when using malicious SSL packets",
      "message": "DoS by CPU exhaustion when using malicious SSL packets in io.netty/netty",
      "description": "The `SslHandler` in this package allows remote attackers to cause a denial of service (infinite loop and CPU consumption) via a crafted `SSLv2Hello` message.",
      "cve": "app/pom.xml:io.netty/netty:gemnasium:d1bf36d9-9f07-46cd-9cfc-8675338ada8f",
      "severity": "Unknown",
      "solution": "Upgrade to the latest version",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "app/pom.xml",
        "dependency": {
          "package": {
            "name": "io.netty/netty"
          },
          "version": "3.9.1.Final"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-d1bf36d9-9f07-46cd-9cfc-8675338ada8f",
          "value": "d1bf36d9-9f07-46cd-9cfc-8675338ada8f",
          "url": "https://deps.sec.gitlab.com/packages/maven/io.netty/netty/versions/3.9.1.Final/advisories"
        },
        {
          "type": "cve",
          "name": "CVE-2014-3488",
          "value": "CVE-2014-3488",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-3488"
        }
      ],
      "links": [
        {
          "url": "http://netty.io/news/2014/06/11/3.html"
        },
        {
          "url": "https://bugzilla.redhat.com/CVE-2014-3488"
        },
        {
          "url": "https://github.com/netty/netty/issues/2562"
        }
      ]
    }
  ],
  "remediations": [],
  "dependency_files": [
    {
      "path": "app/pom.xml",
      "package_manager": "maven",
      "dependencies": [
        {
          "package": {
            "name": "com.fasterxml.jackson.core/jackson-annotations"
          },
          "version": "2.9.0"
        },
        {
          "package": {
            "name": "com.fasterxml.jackson.core/jackson-core"
          },
          "version": "2.9.2"
        },
        {
          "package": {
            "name": "com.fasterxml.jackson.core/jackson-databind"
          },
          "version": "2.9.2"
        },
        {
          "package": {
            "name": "io.netty/netty"
          },
          "version": "3.9.1.Final"
        },
        {
          "package": {
            "name": "junit/junit"
          },
          "version": "3.8.1"
        },
        {
          "package": {
            "name": "org.apache.commons/commons-lang3"
          },
          "version": "3.4"
        },
        {
          "package": {
            "name": "org.apache.maven/maven-artifact"
          },
          "version": "3.3.9"
        },
        {
          "package": {
            "name": "org.codehaus.plexus/plexus-utils"
          },
          "version": "3.0.22"
        },
        {
          "package": {
            "name": "org.hamcrest/hamcrest-core"
          },
          "version": "1.1"
        },
        {
          "package": {
            "name": "org.javassist/javassist"
          },
          "version": "3.21.0-GA"
        },
        {
          "package": {
            "name": "org.mockito/mockito-core"
          },
          "version": "1.10.19"
        },
        {
          "package": {
            "name": "org.objenesis/objenesis"
          },
          "version": "2.1"
        },
        {
          "package": {
            "name": "org.powermock/powermock-api-mockito"
          },
          "version": "1.7.3"
        },
        {
          "package": {
            "name": "org.powermock/powermock-api-mockito-common"
          },
          "version": "1.7.3"
        },
        {
          "package": {
            "name": "org.powermock/powermock-api-support"
          },
          "version": "1.7.3"
        },
        {
          "package": {
            "name": "org.powermock/powermock-core"
          },
          "version": "1.7.3"
        },
        {
          "package": {
            "name": "org.powermock/powermock-reflect"
          },
          "version": "1.7.3"
        }
      ]
    }
  ]
}
